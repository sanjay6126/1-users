const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,                                                
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}



function videoGames(users){
    const result = Object.entries(users).filter((user) => {
        let userProfile =  Object.entries(user[1]);
        let interests = userProfile[2];
        
        let videoGame = interests[1].includes('Video Games')
        if(videoGame == true){
            return user;
        }
    })

    return Object.fromEntries(result);
}

console.log(videoGames(users))





function nationality(users){
    const result = Object.entries(users).filter((user) => {
        return user[1].nationality === "Germany";
    })
    return Object.fromEntries(result);
}

console.log(nationality(users));



function mastersDegree(users){
    const result = Object.entries(users).filter((user) => {
        return user[1].qualification === "Masters"
    })
    return Object.fromEntries(result)
}


console.log(mastersDegree(users))





function sortByDesignation(users){

    let result=Object.entries(users).sort((first,second)=> {
        if(first[1].desgination.includes("Developer")){
            return -1;
        }
        else if(first[1].desgination.includes("Intern")){
            return 1;
        }
    });

    return result;
    // console.log(result);

}

console.log(sortByDesignation(users));





function grouping(users){
    let data = Object.entries(users);               //array of arrays.

    let res = data.reduce((acc,personInfo) => {
        if(acc.hasOwnProperty(personInfo[1].desgination)){
            acc[personInfo[1].desgination].push(personInfo[0]);
        }
        else{
           acc[ personInfo[1].desgination ] = []
           acc[personInfo[1].desgination].push(personInfo[0]);
        }

        return acc;
    },{})
    // console.log(data)

    return res;

}
console.log(grouping(users))
